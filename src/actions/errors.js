import {
  RESET_ERROR_MESSAGE
}  from '../constants/Errors.js';

export const resetErrorMessage = () => ({
  type: RESET_ERROR_MESSAGE
});