import {
  REPORT_INFO_REQUEST,
  REPORT_INFO_FAILURE,
  REPORT_INFO_SUCCESS,
  CREDIT_HISTORY_SUCCESS,
  REPORT_SHOW_FORM,
  REPORT_HIDE_FORM
} from '../constants/ReportInfo.js';
import { Actions } from 'react-native-router-flux';
import { fetchReport, fetchRatingInfo, fetchGuideRaitings } from '../api';

const reportRequest = () => ({
  type: REPORT_INFO_REQUEST
});

const reportSuccess = (report) => ({
  type: REPORT_INFO_SUCCESS,
  payload: {
    report
  }
});

const creditHistorySuccess = (report, guideRaitings) => ({
  type: CREDIT_HISTORY_SUCCESS,
  payload: {
    report,
    guideRaitings
  }
});

export const showForm = () => ({
  type: REPORT_SHOW_FORM
});

export const hideForm = () => ({
  type: REPORT_HIDE_FORM
})

export const lastReport = () => (dispatch, getState) => {
  const state = getState();

  dispatch(reportRequest());
  fetchReport(state.auth.login).then(report =>
    dispatch(reportSuccess(report))
  );
}

export const creditHistory = () => (dispatch) => {
  // Получаем рандомное значение в диапозоне от 1 - 10.
  // const randomId = Math.ceil(Math.random() * 1400);

  dispatch(reportRequest());

  Promise.all([
    fetchRatingInfo(),
    fetchGuideRaitings()
  ]).then(results => {
    dispatch(creditHistorySuccess(results[0], results[1]));
  });
};