import {
  LOGIN_REQUEST,
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
  CHECK_CAPTCHA_FAILURE,
  CHECK_CAPTCHA_SUCCESS,
  LOGIN_CONFIRM_SUCCESS,
  LOGIN_CONFIRM_FAILURE,
  CREATE_PIN_SUCCESS,
  CREATE_PIN_FAILURE,
  USER_LOGOUT,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  CHECK_TOKEN_SUCCESS,
  CHECK_TOKEN_FAILURE,
  AUTH_APP_FAILURE,
  AUTH_USER_FAILURE,
  APP_LOGOUT
}  from '../constants/Auth';
import { Actions } from 'react-native-router-flux';
import DeviceInfo from '../utils/deviceInfo';
import { config } from '../utils/config';
import XML from 'pixl-xml';
import checkHttpStatus from '../utils/checkHttpStatus';
import { stringify } from  'querystring';

const device = DeviceInfo();

const checkStatus = (data, dispatch) => {
  const { status } = data;
  
  try {
    if (status.code === '3') {
      dispatch(authUserFailure());
      Actions.loginUser();
    } else if (status.code === '7') {
      dispatch(authAppFailure());
      Actions.login();
    } else {
      return data;
    }
  } catch (e) {;
    return Promise.reject();
  }
}

export const appLogout = () => ({
  type: APP_LOGOUT
});

export const userLogout = () => ({
  type: USER_LOGOUT
});

export const loginRequest = () => ({
  type: LOGIN_REQUEST
});

export const loginSuccess = (data, login) => ({
  type: LOGIN_SUCCESS,
  payload: {
    data,
    login
  }
});

export const loginFailure = (error) => ({
  type: LOGIN_FAILURE,
  error,
});

export const checkCaptchaSuccess = (mGUID) => ({
  type: CHECK_CAPTCHA_SUCCESS,
  payload: {
    mGUID
  }
});

export const checkCaptchaFailure = (error, captcha) => ({
  type: CHECK_CAPTCHA_FAILURE,
  payload: {
    captcha
  },
  error,
});

/**
 * Подтверждение по смс
 */
export const loginConfirmSuccess = () => ({
  type: LOGIN_CONFIRM_SUCCESS
});

/**
 * Ошибка при подтверждении по смс
 */
export const loginConfirmFailure = (error) => ({
  type: LOGIN_CONFIRM_FAILURE,
  error,
});

/**
 * Пин-код успешно установлен
 */
export const createPinSuccess = () => ({
  type: CREATE_PIN_SUCCESS
});

/**
 * Установка пин-кода вызвало ошибку
 */
export const createPinFailure = (error) => ({
  type: CREATE_PIN_FAILURE,
  error,
});

export const loginUserSuccess = (token) => ({
  type: LOGIN_USER_SUCCESS,
  payload: {
    token
  }
});

export const loginUserFailure = (error) => ({
  type: LOGIN_USER_FAILURE,
  error,
});

export const authAppFailure = () => ({
  type: AUTH_APP_FAILURE
});

export const authUserFailure = () => ({
  type: AUTH_USER_FAILURE
});

/**
 * Проверка токена прошла успешно
 */
export const checkTokenSuccess = () => ({
  type: CHECK_TOKEN_SUCCESS,
});

export const checkTokenFailure = () => ({
  type: CHECK_TOKEN_FAILURE
});

/**
 * Проверка токена
 */
export const checkToken = () => (dispatch, getState) => {
  const state = getState();

  if (state.auth.isAuthentificatedUser && state.auth.token) {
    dispatch(loginRequest());
    return fetch(`${config.host}/mobile${config.version}/private/profile/info.do`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Mobile Device'
      }
    })
    .then(checkHttpStatus)
    .then(response => response.text())
    .then(XML.parse)
    .then(data => {
      if (data.status && data.status.code === '0') {
        dispatch(checkTokenSuccess());
      } else {
        dispatch(checkTokenFailure());
      }
    })
    .catch((error) => {
      dispatch(checkTokenFailure());
    });
  } else {
    dispatch(checkTokenSuccess());
  }
}

// TODO поменять логин на переменную
export const login = (login) => (dispatch) => {
  dispatch(loginRequest());

  return fetch(`${config.host}/CSAMAPI/registerApp.do`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': 'Mobile Device'
    },
    body: stringify({
      operation: 'register',
      login: config.login,
      version: `${config.version}.10`,
      appType: config.appType,
      appVersion: config.appVersion,
      deviceName: config.deviceName,
      devID: config.devID
    })
  })
  .then(checkHttpStatus)
  .then(response => response.text())
  .then(XML.parse)
  .then(data => checkStatus(data, dispatch))
  .then(data => {
    const { status } = data;

    if (status && status.code === '0') {
      // TODO раскоментировать когда протестирую все
      //dispatch(loginSuccess(data, login));
      dispatch(loginSuccess(data, config.login));

      if (data.captchaRegistrationStage) {
        Actions.captcha();
      }
      if (data.confirmRegistrationStage) {
        Actions.loginConfirm();
      }
    }

    if (status && (status.code === '1' || status.code === '2' || status.code === '8')) {
      dispatch(loginFailure(status.errors.error.text));
    }
  })
  .catch((error) => dispatch(loginFailure('Произошла системная ошибка, повторите запрос позже!')));
}

export const loginConfirm = (smsPassword) => (dispatch, getState) => {
  const state = getState();
  
  dispatch(loginRequest());
  return fetch(`${config.host}/CSAMAPI/registerApp.do`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': 'Mobile Device'
    },
    body: stringify({
      operation: 'confirm',
      mGUID: state.auth.mGUID,
      smsPassword: config.smsPassword,
      version: `${config.version}.10`,
      appType: config.appType
    })
  })
  .then(checkHttpStatus)
  .then(response => response.text())
  .then(XML.parse)
  .then(data => checkStatus(data, dispatch))
  .then(data => {
    const { status } = data;

    if (status && status.code === '0') {
      dispatch(loginConfirmSuccess());
      Actions.createPin();
    }
    
    if (status && (status.code === '1' || status.code === '2' || status.code === '8')) {
      dispatch(loginConfirmFailure(status.errors.error.text));
    }
  })
  .catch((data) => dispatch(loginConfirmFailure('Произошла системная ошибка, повторите запрос позже!')));
}

export const checkCaptcha = (captcha) => (dispatch, getState) => {
  const state = getState();
  
  dispatch(loginRequest());
  
  return fetch (`${config.host}/CSAMAPI/registerApp.do`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': 'Mobile Device'
    },
    body: stringify({
      operation: 'checkCaptcha',
      captcha: captcha,
      login: state.auth.login,
      version: `${config.version}.10`,
      appType: config.appType,
      devID: config.devID
    })
  })
  .then(checkHttpStatus)
  .then(response => response.text())
  .then(XML.parse)
  .then(data => checkStatus(data, dispatch))
  .then(data => {
    const { status, confirmRegistrationStage, captchaRegistrationStage } = data;

    if (status && status.code === '0') {
      dispatch(checkCaptchaSuccess(confirmRegistrationStage.mGUID));
      Actions.loginConfirm(); 
    }

    if (captchaRegistrationStage && status.code === '1') {
      dispatch(checkCaptchaFailure(status.errors.error.text, captchaRegistrationStage.captcha));
    }
  })
  .catch((data) => dispatch(checkCaptchaFailure('Произошла системная ошибка, повторите запрос позже!')));
}

export const createPin = (password) => (dispatch, getState) => {
  const state = getState();
  
  dispatch(loginRequest());
  return fetch(`${config.host}/CSAMAPI/registerApp.do`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': 'Mobile Device'
    },
    body: stringify({
      operation: 'createPIN',
      mGUID: state.auth.mGUID,
      password: config.password,
      version: `${config.version}.10`,
      appType: config.appType,
      appVersion: config.appVersion,
      deviceName: config.deviceName,
      devID: config.devID,
      mobileSdkData: '123'
    })
  })
  .then(checkHttpStatus)
  .then(response => response.text())
  .then(XML.parse)
  .then(data => checkStatus(data, dispatch))
  .then(data => {
    const { status } = data;

    if (status && status.code === '0') {
      dispatch(createPinSuccess());
      Actions.loginUser();
    }

    if (status && (status.code === '1' || status.code === '2' || status.code === '8')) {
      dispatch(createPinFailure(status.errors.error.text));
    }
    
  })
  .catch((error) => dispatch(createPinFailure('Произошла системная ошибка, повторите запрос позже!')));
}

export const loginUser = (password) => (dispatch, getState) => {
  const state = getState();

  dispatch(loginRequest());
  return fetch(`${config.host}/CSAMAPI/login.do`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': 'Mobile Device'
    },
    body: stringify({
      operation: 'button.login',
      mGUID: state.auth.mGUID,
      password: config.password,
      version: `${config.version}.10`,
      appType: config.appType,
      appVersion: config.appVersion,
      deviceName: config.deviceName,
      devID: config.devID,
      isLightScheme: false
    })
  })
  .then(checkHttpStatus)
  .then(response => response.text())
  .then(XML.parse)
  .then(data => checkStatus(data, dispatch))
  .then(data => {
    const { status, loginData } = data;

    if (status && status.code === '0') {
      if (loginData && loginData.token) {
        dispatch(loginUserAuthToToken(loginData.token));
      } else {
        dispatch(loginUserFailure('Произошла ошибка данных, повторите запрос позже'));
      }
    } 

    if (status && (status.code === '1' || status.code === '2' || status.code === '8')) {
      dispatch(loginUserFailure(status.errors.error.text));
    }
  })
  .catch((error) => dispatch(loginUserFailure('Произошла системная ошибка, повторите запрос позже!')));
}

const loginUserAuthToToken = (token) => (dispatch) => {
  dispatch(loginRequest());
  return fetch(`${config.host}/mobile${config.version}/postCSALogin.do`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': 'Mobile Device'
    },
    body: stringify({
      token :token
    })
  })
  .then(checkHttpStatus)
  .then(response => response.text())
  .then(XML.parse)
  .then(data => checkStatus(data, dispatch))
  .then(data => {
    const { status } = data;

    if (status && status.code === '0') {
      dispatch(loginUserSuccess(token));
    }

    if (status && (status.code === '1' || status.code === '2' || status.code === '8')) {
      dispatch(loginUserFailure(status.errors.error.text));
    }
    
  })
  .catch((error) => dispatch(loginUserFailure('Произошла системная ошибка, повторите запрос позже!')));
}