import {
  REPORT_INFO_REQUEST,
  REPORT_INFO_FAILURE,
  REPORT_INFO_SUCCESS,
  CREDIT_HISTORY_SUCCESS,
  REPORT_SHOW_FORM,
  REPORT_HIDE_FORM
} from '../constants/ReportInfo.js';
import find from 'lodash/find';

const initialState = {
  isReadyReport: false,
  isShowForm: false,
  isFetching: false,
  report: {},
  ratingInfo: null,
  lastRating: null
};

const reportInfo = (state = initialState, action) => {
  switch (action.type) {
    case REPORT_INFO_REQUEST:
      return {
        ...state,
        isFetching: true
      }
    case REPORT_INFO_SUCCESS:
      if (action.payload.report) {
        return {
          ...state,
          isReadyReport: true,
          isFetching: false,
          report: action.payload.report
        }
      }
      return {
        ...state,
        ...initialState
      }
    case CREDIT_HISTORY_SUCCESS:
      const { guideRaitings, report } = action.payload;
      const ratingInfo = find(guideRaitings, (item) => report.ratingIndicator >= item.minIndicator && report.ratingIndicator <= item.maxIndicator);
      report.date = new Date();

      return {
        ...state,
        isReadyReport: true,
        isFetching: false,
        isShowForm: false,
        report: report,
        ratingInfo,
      }
    case REPORT_SHOW_FORM:
      return {
        ...state,
        isShowForm: true
      }  
    case REPORT_HIDE_FORM:
      return {
        ...state,
        isShowForm: false
      }
    default:
      return state;
  }
};

export default reportInfo;