import {
  LOGIN_REQUEST,
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
  CHECK_CAPTCHA_FAILURE,
  CHECK_CAPTCHA_SUCCESS,
  LOGIN_CONFIRM_SUCCESS,
  LOGIN_CONFIRM_FAILURE,
  CREATE_PIN_SUCCESS,
  CREATE_PIN_FAILURE,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  USER_LOGOUT,
  CHECK_TOKEN_SUCCESS,
  CHECK_TOKEN_FAILURE,
  AUTH_APP_FAILURE,
  AUTH_USER_FAILURE,
  APP_LOGOUT
}  from '../constants/Auth';
import { REHYDRATE } from 'redux-persist/constants';

const initialState = {
  isAuthentificatedUser: false,
  isAuthentificatedApp: false,
  login: null,
  token: null,
  mGUID: null,
  captcha: null,
  isFetching: false,
  isCheckToken: false
};

const authState = (state = initialState, action) => {
  switch (action.type) {
    case REHYDRATE :
      if (action.payload.auth && action.payload.auth.isAuthentificatedApp && action.payload.auth.mGUID) {
        return {
          ...state,
          isAuthentificatedUser: action.payload.auth.isAuthentificatedUser,
          isAuthentificatedApp: action.payload.auth.isAuthentificatedApp,
          login: action.payload.auth.login,
          mGUID: action.payload.auth.mGUID,
          token: action.payload.auth.token,
        }
      }
      return {
        ...state,
        isAuthentificatedUser: false,
        isAuthentificatedApp: false,
        isFetching: false,
      };
    case APP_LOGOUT:
      return {
        ...state,
        isAuthentificatedUser: false,
        isAuthentificatedApp: false,
        token: null,
        mGUID: null,
        captcha: null,
        login: null,
        isFetching: false,
      };
    case USER_LOGOUT:
      return {
        ...state,
        isAuthentificatedUser: false,
        token: null,
        isFetching: false,
      }
    case AUTH_APP_FAILURE:
      return {
        ...state,
        isAuthentificatedUser: false,
        isAuthentificatedApp: false,
        login: null,
        token: null,
        mGUID: null,
        isFetching: false,
      };
      
    case AUTH_USER_FAILURE:
      return {
        ...state,
        isAuthentificatedUser: false,
        token: null,
        isFetching: false,
      };

    case LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case LOGIN_SUCCESS:
      const {data, login} = action.payload;
      let captcha = null;
      let mGUID = null;

      if (data.captchaRegistrationStage && data.captchaRegistrationStage.captcha) {
        captcha = data.captchaRegistrationStage.captcha;
      }

      if (data.confirmRegistrationStage && data.confirmRegistrationStage.mGUID) {
        mGUID = data.confirmRegistrationStage.mGUID;
      }

      return {
        ...state,
        mGUID,
        login,
        captcha,
        isFetching: false
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        isFetching: false
      };

    case CHECK_CAPTCHA_SUCCESS:
      return {
        ...state,
        mGUID: action.payload.mGUID,
        isFetching: false,
        captcha: null,
      };

    case CHECK_CAPTCHA_FAILURE:
      return {
        ...state,
        captcha: action.payload.captcha,
        isFetching: false
      };

    case LOGIN_CONFIRM_SUCCESS: {
      return {
        ...state,
        isFetching: false
      }
    }
    case CREATE_PIN_SUCCESS: {
      return {
        ...state,
        isFetching: false,
        isAuthentificatedApp: true
      };
    }
    case CREATE_PIN_FAILURE: {
      return {
        ...state,
        isFetching: false
      };
    }
    case LOGIN_USER_SUCCESS: {
      return {
        ...state,
        token: action.payload.token,
        isAuthentificatedUser: true,
        isFetching: false
      };
    }
    case LOGIN_USER_FAILURE: {
      return {
        ...state,
        token: null,
        isAuthentificatedUser: false,
        isFetching: false
      };
    }
    case CHECK_TOKEN_SUCCESS: {
      return {
        ...state,
        isCheckToken: true,
        isFetching: false
      };
    }
    case CHECK_TOKEN_FAILURE: {
      return {
        ...state,
        isCheckToken: true,
        isAuthentificatedUser: false,
        token: null,
        isFetching: false
      };
    }
    
    default:
      return state;
  }
};

export default authState;