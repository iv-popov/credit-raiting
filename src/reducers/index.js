import { combineReducers } from 'redux';
import routes from './routes';
import auth from './auth';
import reportInfo from './reportInfo';
import errorMessage from './errorMessage';

export default combineReducers({
  routes,
  auth,
  reportInfo,
  errorMessage
});

export const isAllLoaded = (state) => state.auth.isCheckToken;