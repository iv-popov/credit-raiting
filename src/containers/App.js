import React, { Component, PropTypes } from 'react';
import { Alert, Text } from 'react-native';

import Routes from '../routes';
import { resetErrorMessage } from '../actions/errors';
import { userLogout } from '../actions/auth';
import { isAllLoaded } from '../reducers';
import BusyIndicator from '../components/BusyIndicator';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

class App extends Component {
  static propTypes = {
    errorMessage: PropTypes.string,
    resetErrorMessage: PropTypes.func.isRequired,
    userLogout: PropTypes.func.isRequired
  };

  componentWillReceiveProps = (props) => {
    const { errorMessage } = props;

    if (errorMessage) {
      Alert.alert(
        'Ошибка',
        errorMessage,
        [
          {text: 'OK', onPress: () => this.props.resetErrorMessage()}
        ]
      );
    }
  };

  handleUserLogout = () => {
    Alert.alert(
      'Закрытие программы',
      'Вы действительно хотите выйти?',
      [
        {text: 'ОТМЕНА', style: 'cancel'},
        {text: 'OK', onPress: () => {
          this.props.userLogout();
          Actions.login();
        }},
      ]
    )
  }

  render() {
    if(!this.props.loaded) {
      return <BusyIndicator />
    }

    return (
      <Routes onLogoutUser={this.handleUserLogout} />
    );
  }
}

const mapStateToProps = (state) => ({
  loaded: isAllLoaded(state),
  errorMessage: state.errorMessage
});

const mapDispatchToProps = (dispatch) => ({
  resetErrorMessage() {
    dispatch(resetErrorMessage());
  },
  userLogout() {
    dispatch(userLogout());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);