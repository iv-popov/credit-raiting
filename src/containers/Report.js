import React, { Component, PropTypes } from 'react';
import { ScrollView, Text, BackAndroid, Platform, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import * as ReportActions from '../actions/reportInfo';

import BusyIndicator from '../components/BusyIndicator';
import ReportEmpty from '../components/report/ReportEmpty';
import Form from '../components/report/Form';
import Rating from '../components/report/Rating';

class Report extends Component {
  static propTypes = {
    auth: PropTypes.object,
    reportInfo: PropTypes.object,
    reportActions: PropTypes.object.isRequired
  };

  render() {
    const { reportInfo, reportActions: { creditHistory, showForm, hideForm } } = this.props;

    if (reportInfo.isFetching) {
      return <BusyIndicator />;
    }

    if (reportInfo.isShowForm) {
      return <Form onSubmit={creditHistory} onHideForm={hideForm} />
    }
    return (
      <ScrollView style={styles.container}>
        <Text>Кредитный отчет - это документ, который отражает Вашу кредитную историю, то есть обобщенную информацию по кредитным обязательствам и их состоянию на текущий момент.</Text>
        {reportInfo.isReadyReport ? <Rating reportInfo={reportInfo} onShowForm={showForm} /> : <ReportEmpty onShowForm={showForm}/>}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    marginTop: 70,
    ...Platform.select({
      ios: {
        marginBottom: 50
      }
    })
  },
});

const mapStateToProps = (state) => ({
  auth: state.auth,
  reportInfo: state.reportInfo
});

const mapDispatchToProps = (dispatch) => ({
  reportActions: bindActionCreators(ReportActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Report);