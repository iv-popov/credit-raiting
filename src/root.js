import React from 'react';
import { Text, AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import App from './containers/App';
import { persistStore } from 'redux-persist';
import { checkToken } from './actions/auth';
// import codePush from "react-native-code-push";

const store = configureStore();

persistStore(store, {
  whitelist: ['auth', 'errorMessage'],
  storage: AsyncStorage
}, () => {
  store.dispatch(checkToken());
});

class Root extends React.Component {
  /*componentDidMount(){
    codePush.sync({ updateDialog: {appendReleaseDescription: true}, installMode: codePush.InstallMode.IMMEDIATE });
  }*/
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

export default Root;