import React, { PropTypes, Component } from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as AuthActions from '../../actions/auth';
import BusyIndicator from '../BusyIndicator';
import AuthHeader from './AuthHeader';

import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import dismissKeyboard from 'react-native-dismiss-keyboard';

class AuthLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
    }
  }
  
  static propTypes = {
    routes: PropTypes.object,
    auth: PropTypes.object,
    authActions: PropTypes.object.isRequired
  };

  onLogin = () => {
    if (this.state.login.length > 2) {
      dismissKeyboard();
      this.props.authActions.login(this.state.login);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <AuthHeader />
        <View style={{flexDirection: "row", marginHorizontal: 20}}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.textInput}
              placeholder="Логин или индентификатор"
              underlineColorAndroid="transparent"
              onChangeText={(login) => this.setState({login})}
              value={this.state.login}
            />
          </View>
          <Icon name="arrow-circle-right" color={this.state.login.length > 2 ? "#299E30" : null } size={35} onPress={this.onLogin}/>
        </View>
        { this.props.auth.isFetching &&
            <BusyIndicator />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputContainer: {
    flex: 1,
    height: 40,
    padding: 0,
    marginRight: 60,
    borderBottomColor: '#299E30',
    borderBottomWidth: 2
  },
  textInput: {
    height: 40,
    fontSize: 16
  }
});

const mapStateToProps = (state) => ({
  auth: state.auth,
  routes: state.routes
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(AuthActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthLogin);