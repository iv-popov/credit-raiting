import React, { PropTypes, Component } from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as AuthActions from '../../actions/auth';
import BusyIndicator from '../BusyIndicator';
import AuthHeader from './AuthHeader';

import Icon from 'react-native-vector-icons/FontAwesome';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import { Actions } from 'react-native-router-flux';

class AuthLoginConfirm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      smsCode: '',
    }
  }
  
  static propTypes = {
    routes: PropTypes.object,
    auth: PropTypes.object,
    authActions: PropTypes.object.isRequired,
  };

  handleLoginConfirm = () => {
    if (this.state.smsCode.length === 5) {
      dismissKeyboard();
      this.props.authActions.loginConfirm(this.state.smsCode);
    }
  };

  handleAuthAppCancel = () => {
    this.props.authActions.appLogout();
    Actions.login();
  };

  render() {
    return (
      <View style={styles.container}>
        <AuthHeader onAuthAppCancel={this.handleAuthAppCancel} />
        <View style={{flexDirection: "row", marginHorizontal: 20}}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.textInput}
              keyboardType='numeric'
              placeholder="Введите код из смс"
              underlineColorAndroid="transparent"
              onChangeText={(smsCode) => this.setState({smsCode})}
              value={this.state.smsCode}
            />
          </View>
          <Icon name="arrow-circle-right" color={this.state.smsCode.length === 5 ? "#299E30" : null } size={35} onPress={this.handleLoginConfirm}/>
        </View>
        { this.props.auth.isFetching &&
            <BusyIndicator />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputContainer: {
    flex: 1,
    height: 40,
    padding: 0,
    marginRight: 60,
    borderBottomColor: '#299E30',
    borderBottomWidth: 2
  },
  textInput: {
    height: 40,
    fontSize: 16
  }
});

const mapStateToProps = (state) => ({
  auth: state.auth
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(AuthActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoginConfirm);