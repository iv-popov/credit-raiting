import React, { PropTypes, Component } from 'react';
import {
  View,
  Image,
  TextInput,
  Text,
  StyleSheet
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as AuthActions from '../../actions/auth';
import BusyIndicator from '../BusyIndicator';
import AuthHeader from './AuthHeader';

import Icon from 'react-native-vector-icons/FontAwesome';
import dismissKeyboard from 'react-native-dismiss-keyboard';
import { Actions } from 'react-native-router-flux';

class AuthCaptcha extends Component {
  constructor(props) {
    super(props);
    this.state = {
      captcha: '',
    }
  }

  static propTypes = {
    auth: PropTypes.object,
    authActions: PropTypes.object.isRequired
  };

  checkCaptcha = () => {
    if (this.state.captcha.length > 0) {
      dismissKeyboard();
      this.props.authActions.checkCaptcha(this.state.captcha);
    }
  }

  handleAuthAppCancel = () => {
    this.props.authActions.appLogout();
    Actions.login();
  };

  render () {
    return (
      <View style={styles.container}>
        <AuthHeader onAuthAppCancel={this.handleAuthAppCancel} />
        <View style={{flexDirection: "row", marginHorizontal: 20}}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.textInput}
              placeholder="Введите код с картинки"
              underlineColorAndroid="transparent"
              onChangeText={(captcha) => this.setState({captcha})}
              value={this.state.captcha}
            />
          </View>
          <Icon name="arrow-circle-right" color={this.state.captcha.length > 0 ? "#299E30" : null } size={35} onPress={this.checkCaptcha}/>
        </View>
        <Image style={styles.base64} source={{uri: `data:image/png;base64,${this.props.auth.captcha}`}} />
        { this.props.auth.isFetching &&
            <BusyIndicator />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputContainer: {
    flex: 1,
    height: 40,
    padding: 0,
    marginRight: 60,
    borderBottomColor: '#299E30',
    borderBottomWidth: 2
  },
  textInput: {
    height: 40,
    fontSize: 16
  },
  base64: {
    marginTop: 20,
    height: 90,
    resizeMode: 'contain',
  },
});

const mapStateToProps = (state) => ({
  auth: state.auth
});

const mapDispatchToProps = (dispatch) => ({
  authActions: bindActionCreators(AuthActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthCaptcha);