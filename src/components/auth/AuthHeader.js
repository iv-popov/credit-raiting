import React, {Component} from 'react';
import { View, Text, Platform, StyleSheet } from 'react-native';

import Button from 'react-native-button';

const AuthHeader = (props) => (
<View>
  {Platform.OS === 'ios' && <View style={styles.statusBarIOS}></View>}
  <View style={styles.header}>
    <Text style={styles.header__t}>Кредитный рейтинг</Text>
    { props.onAuthAppCancel &&
      <Button
        style={styles.header__lk}
        onPress={props.onAuthAppCancel}>
        ОТМЕНА
      </Button>
    }
    { props.onAuthAppLogout &&
      <Button
        style={styles.header__lk}
        onPress={props.onAuthAppLogout}>
        ВЫХОД
      </Button>
    }      
  </View>
  </View>
);

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    marginBottom: 50,
    marginHorizontal: 20,
  },
  statusBarIOS: {
    height: 22,
    backgroundColor: '#299E30'
  },
  header__t: {
    flex: 1,
    marginVertical: 10,
    fontWeight: 'bold',
    fontSize: 20,
    color: '#299E30'
  },
  header__lk: {
    marginTop: 17,
    fontSize: 14,
    color: '#ef7a1b',
    fontWeight: 'bold',
  }
});

export default AuthHeader;