import React, {Component} from 'react';
import { ScrollView, Text, BackAndroid, StatusBar, Platform, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';

class Home extends Component {
  render() {
    return (
      <ScrollView  style={styles.container}>
        <Text style={styles.heading}>
          Что такое кредитный рейтинг физических лиц
        </Text>
        <Text style={styles.paragraph}>При рассмотрении заявки на кредит, банки анализируют ряд параметров для оценки кредитоспособности потенциального заёмщика. В результате формируется персональный кредитный рейтинг.</Text>
        <Text style={styles.paragraph}>Кредитный рейтинг — показатель вашей платежеспособности, исчисляющийся в баллах (кредитный скоринг) на который опирается любое банковское учреждение и микрофинансовая организация при вынесении решения о выдаче кредита. Кредитный рейтинг показывает насколько хороша Ваша кредитная история и требуется ли ее улучшение. В кредитном рейтинге указывается конфиденциальная информация о физических лицах имеющаяся в банковских базах, на основе которой банк принимает решение о выдаче кредита.</Text>
        <Text style={styles.paragraph}>Каждая кредитная организация по-своему относится к одной и той же кредитной истории. Ваш Персональный кредитный рейтинг позволит узнать к какой группе клиентов Вы относитесь, и какова вероятность одобрения Вашей заявки.</Text>
        <Text style={styles.heading}>Для чего нужно знать свой кредитный рейтинг</Text>
        <Text style={styles.paragraph}>И ТАК, ВЫ БУДЕТЕ:</Text>
        <Text style={styles.paragraph}>
          <Text style={styles.listHeading}>1. Знать и понимать причину отказа в выдаче кредита.{"\n"}</Text>
          <Text style={styles.listText}>Вы будете знать почему банки отказывают вам в выдаче займа и с можете исправить это в своих интересах.</Text>
        </Text>
        <Text style={styles.paragraph}>
          <Text style={styles.listHeading}>2. Быстро и своевременно выявлять мошеннические действий связанные с вашими личными данными.{"\n"}</Text>
          <Text style={styles.listText}>Оформление кредита на ваши личные данные без вашего участия и согласия.</Text>
        </Text>
        <Text style={styles.paragraph}>
          <Text style={styles.listHeading}>3. Знать все проблемы связанные с вашими не активированными кредитными картами которые оформлены на ваши личные данные.{"\n"}</Text>
          <Text style={styles.listText}>Не активированные кредитные карты зарегистрированные на ваше имя могут числится как действующий займ.</Text>
        </Text>
        <Text style={styles.paragraph}>
          <Text style={styles.listHeading}>4. Иметь возможность выявления и исправления ошибок которые допускает банковское учреждение.{"\n"}</Text>
          <Text style={styles.listText}>Да, да и это возможно, статистика выявляет огромное количество ошибок которые допустил банк, и соответственно ваши шансы в получении кредита также уменьшаются из-за ошибок которые вы не совершали.</Text>
        </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    marginTop: 70,
    ...Platform.select({
      ios: {
        marginBottom: 50
      }
    })
  },
  heading: {
    paddingBottom: 10,
    fontSize: 18,
    fontWeight: '700',
    fontStyle: 'italic',
    
  },
  paragraph: {
    paddingBottom: 5
  },
  listHeading: {
    color: '#ef7a1b'
  },
  listText: {
    fontStyle: 'italic'
  }
})

export default Home;