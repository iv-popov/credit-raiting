import React from 'react';
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native';

const BusyIndicator = () => (
  <View style={styles.containerProgressBar}>
    <View style={styles.overlayProgressBar}>
      <ActivityIndicator
        color="#f5f5f5"
        size="small"
        style={styles.progressBar}/>
      <Text numberOfLines={1} style={styles.textProgressBar}>
        Подождите...
      </Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
   containerProgressBar: {
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.3)',
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    flex: 1
  },
  overlayProgressBar: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    padding: 10,
    backgroundColor: "#333333",
    width: 120,
    height: 100
  },
  textProgressBar: {
    color:"#f5f5f5",
    fontSize: 14,
    marginTop: 8
  },
  progressBar: {
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  }
});

export default BusyIndicator;