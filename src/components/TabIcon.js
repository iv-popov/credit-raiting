import React, { PropTypes } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

const TabIcon = (props) => (
  <Icon name={props.selected ? props.iconNameActive : props.iconName } size={30} color="#fff"/>
);

TabIcon.propTypes = {
  selected: PropTypes.bool,
  iconNameActive: PropTypes.string,
  iconName: PropTypes.string,
};

export default TabIcon;
