import React, { PropTypes } from 'react';
import { StyleSheet, Text, View } from "react-native";
import Button from 'react-native-button';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';

const contextTypes = {
  drawer: React.PropTypes.object,
};

const propTypes = {
  name: PropTypes.string,
  sceneStyle: View.propTypes.style,
  title: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  textStyle: {
    fontSize: 16
  }
});

const TabView = (props, context) => {
  const drawer = context.drawer;

  return (
    <View style={[styles.container]}>
      <Icon.Button
        name="md-home"
        backgroundColor="#F5FCFF"
        color="#299E30"
        borderRadius={0}
        size={25}
        underlayColor="white"
        onPress={() => { drawer.close(); Actions.main(); }}
      >
        <Text style={styles.textStyle}>Главная</Text>
      </Icon.Button>
      <Icon.Button
        name="md-paper"
        backgroundColor="#F5FCFF"
        color="#299E30"
        borderRadius={0}
        size={25}
        underlayColor="white"
        onPress={() => { drawer.close(); Actions.report(); }}
      >
        <Text style={styles.textStyle}>Отчет</Text>
      </Icon.Button>
      <Icon.Button
        name="md-information-circle"
        backgroundColor="#F5FCFF"
        color="#299E30"
        borderRadius={0}
        size={25}
        underlayColor="white"
        onPress={() => { drawer.close(); Actions.recommendation(); }}
      >
        <Text style={styles.textStyle}>Рекомендации</Text>
      </Icon.Button>
    </View>
  );
};

TabView.contextTypes = contextTypes;
TabView.propTypes = propTypes;

export default TabView;