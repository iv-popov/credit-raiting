import React, { Component, PropTypes } from 'react';
import { ART, Dimensions, Platform, View } from 'react-native';
import * as shape from 'd3-shape';

import Svg,{
    G,
    Path,
    Text,
    Circle,
} from 'react-native-svg';

import Morph from 'art/morph/path';
const dimensionWindow = Dimensions.get('window');
const PaddingSize = 20;

class ArcGauge extends Component {
  static propTypes = {
    value: React.PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    sections: React.PropTypes.any,
    arrow: React.PropTypes.object,
    size: PropTypes.number,
    radius: React.PropTypes.number,
    legend: React.PropTypes.any
  }

  static defaultProps = {
    width: Math.round(dimensionWindow.width * 0.9),
    height: Math.round(dimensionWindow.height * 0.5),
    size: 20,
    radius: 100,
    sections: ['#ccc', '#999', '#444'],
    legend: null,
    arrow: {
      height: 55,
      width: 5,
      color: '#a5a5a5'
    }
  }

  componentWillMount() {
    this.renderArcGauge(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.renderArcGauge(nextProps);
  }

  renderArcGauge(nextProps) {
    const {
      width,
      height,
      sections,
      radius,
      size,
      value,
      arrow,
      legend
    } = nextProps;

    const graphWidth = width - PaddingSize * 2;
    const graphHeight = height - PaddingSize * 2;
    const arc = shape.arc();

    let sectionsNum = sections.length,
        rotateAngle = .75,
        sectionFill = 1 / sectionsNum / 2,
        sectionSpaces = 0.05, 
        arcStart, arcEnd, padStart, padEnd,
        arrowPath,
        arrActs = [];

    sections.map((section, index) => {
      arcStart = this.percToRad(rotateAngle);
      arcEnd = arcStart + this.percToRad(sectionFill);
      rotateAngle += sectionFill;
      padStart = 0 ? 0 : sectionSpaces / 2;
      padEnd = sectionsNum ? 0 : sectionSpaces / 2;

      arrActs.push({
        arc: arc({
          innerRadius: radius - size,
          outerRadius: radius,
          startAngle: arcStart + padStart,
          endAngle: arcEnd - padEnd
        }),
        arcText: arc({
          innerRadius: radius - size - 30,
          outerRadius: radius - 30,
          startAngle: arcStart + padStart,
          endAngle: arcEnd - padEnd
        }),
        fill: section,
        index: index.toString()
      })
    })

    arrowPath = this.mkCmd(arrow.width, arrow.height, value * 0.01);

    this.setState({
      graphWidth,
      graphHeight,
      arrActs,
      arrow,
      arrowPath,
      legend
    });
  }

  mkCmd = (width, height, perc) => {
    let centerX, centerY, leftX, leftY, rightX, rightY, thetaRad, topX, topY;

    thetaRad = this.percToRad(perc / 2);
    centerX = 0;
    centerY = 0;
    topX = centerX - height * Math.cos(thetaRad);
    topY = centerY - height * Math.sin(thetaRad);
    leftX = centerX - width * Math.cos(thetaRad - Math.PI / 2);
    leftY = centerY - width * Math.sin(thetaRad - Math.PI / 2);
    rightX = centerX - width * Math.cos(thetaRad + Math.PI / 2);
    rightY = centerY - width * Math.sin(thetaRad + Math.PI / 2);
    return "M " + leftX + " " + leftY + " L " + topX + " " + topY + " L " + rightX + " " + rightY;
  }

  percToDeg(perc) {
    return perc * 360;
  }

  percToRad(perc) {
    return this.degToRad(this.percToDeg(perc));
  }

  degToRad(deg) {
    return deg * Math.PI / 180.5;
  }

  deg2rad(deg) {
    return deg / 180 * Math.PI;
  }

  render() {
    const { graphWidth, graphHeight, arrActs, arrow, arrowPath, legend } = this.state;
  
    return (
       <Svg width={graphWidth} height={graphHeight/2+20}>
        <G translateX={graphWidth/2} translateY={graphHeight/2}>
          {arrActs.map((item, index) => (
            <G key={index}>
              <Path
                d={item.arc}
                fill={item.fill}
              />
            <Text path={item.arcText} fill="black" x={0} y={Platform.OS === 'ios' ? 5 : 10} fontWeight="bold" fontSize="11">{"  " + legend[item.index]}</Text>
            </G>
          ))}
          <Circle
            cx="0"
            cy="0"
            r={arrow.width}
            fill={arrow.color}
          />
          <Path d={arrowPath} fill={arrow.color} />
        </G>
      </Svg>
    );
  }
}

export default ArcGauge;