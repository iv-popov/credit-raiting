import React, { Component, PropTypes } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class ReportEmpty extends Component {
  static propTypes = {
    onShowForm: PropTypes.func.isRequired
  }

  render() {
    return (
      <View>
        <Text style={styles.noRequestText}>Запрос на получение кредитной истории не производился.</Text>
        
        <View style={styles.btnContainer}>
          <Icon.Button name="ios-card" backgroundColor="#299E30" onPress={this.props.onShowForm}>
            Получить кредитную историю 
          </Icon.Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  btnContainer: {
    marginTop: 10,
    alignItems: 'center'
  },
  noRequestText: {
    fontWeight: '700',
    marginVertical: 10,
    textAlign: 'center',
    fontSize: 16
  }
});

export default ReportEmpty;