import React, { Component, PropTypes } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Collapsible from 'react-native-collapsible';

class Box extends Component {
  state = {
    collapsed: true
  }

  static propTypes = {
    contract: PropTypes.object.isRequired,
    bgColor: PropTypes.string.isRequired,
    isDetail: PropTypes.bool
  }

  toggleDetailInfo = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  render() {
    const { contract, bgColor, isDetail } = this.props;
    return (
      <View>
        <View style={[styles.box, {backgroundColor: bgColor}]}>
          <View style={styles.box__header}>
            <Text style={styles.box__smFS}>{contract.bank_name.toUpperCase()}</Text>
            <Text style={styles.box__smFS}><Text style={styles.box__heading}>{contract.creditPurpose}</Text> / {contract.summa} <Icon name="rub" size={12} color="#777" /> / {contract.period}</Text>
          </View>
          {contract.active && 
            <View style={styles.box__cnt}>
              <View style={styles.box__cntCol}>
                <Text>ВСЕГО К ВЫПЛАТЕ</Text>
                <Text style={styles.box__heading}>{contract.rest} <Icon name="rub" size={15} color="#777" /></Text>
              </View>
              <View style={styles.box__cntCol}>
                <Text>ПЛАТЕЖ</Text>
                <Text style={styles.box__heading}>{contract.payment_month} <Icon name="rub" size={15} color="#777" /></Text>
              </View>
            </View>
          }
          {!contract.active && 
            <View>
              <Text>ПРИЧИНА ЗАКРЫТИЯ{"\n"}</Text>
              <Text>{contract.closing_reason}</Text>
            </View>
          }
          {isDetail &&
            <TouchableOpacity onPress={this.toggleDetailInfo}>
              <Text style={styles.box__detailLk}>{this.state.collapsed ? 'Показать' : 'Скрыть'} информацию о договоре</Text>
            </TouchableOpacity>
          }
        </View>
        {isDetail && 
          <Collapsible collapsed={this.state.collapsed}>
            <View style={styles.box__detailCnt}>
              <Text style={[styles.box__heading, {marginBottom: 10}]}>Общие сведения о кредитном договоре</Text>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Кредитор</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.lender}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Тип финансирования</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.typeFinancing}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Тип обеспечения</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.typeSecurity}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Тип владельца</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.typeOwner}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Срок</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.time}</Text>
                </View>
              </View>
              <View style={styles.box__hr}></View>  
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Дата открытия</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.dateOpen}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Дата последнего платежа</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.dateLastPayment}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Последний пропущенный платеж</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.dateMissedPayment}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Дата исполнение обязательств</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.dateEnd}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Дата закрытия (фактическая)</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.dateClose}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Дата судебного рассмотрения</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.dateTrial}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Дата списания</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.dateDeduction}</Text>
                </View>
              </View>
              <View style={styles.box__hr}></View> 
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Валюта</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.currency}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Сумма кредита</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.summa}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Размер платежа</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.payment_month}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Всего к выплате</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.rest}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Сумма просрочки</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.generalInfo.amountDelay}</Text>
                </View>
              </View>
              <View style={styles.box__hr}></View> 
              <Text style={[styles.box__heading, {marginBottom: 10}]}>Источник кредитной истории</Text>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>Наименование</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.source.name}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>ОГРН</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.source.ogrn}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>ИНН</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.source.inn}</Text>
                </View>
              </View>
              <View style={styles.box__detailRow}>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>ОКПО</Text>
                </View>
                <View style={styles.box__detailCol}>
                  <Text style={styles.box__detailTx}>{contract.detailInfo.source.okpo}</Text>
                </View>
              </View>
            </View>
          </Collapsible>
        }
      </View>  
    );
  }
}

const styles = StyleSheet.create({
  box: {
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 5,
    padding: 10,
  },
  box__header: {
    marginBottom: 10
  },
  box__heading: {
    fontWeight: '700',
    fontSize:16
  },
  box__smFS: {
    fontSize: 12
  },
  box__cnt: {
    flexDirection: 'row'
  },
  box__hr: {
    marginVertical: 10,
    borderBottomWidth: 1,
    borderColor: '#ddd'
  },
  box__cntCol: {
    flex: 2
  },
  box__detailLk: {
    marginTop: 10,
    textDecorationLine: 'underline'
  },
  box__detailCnt: {
    marginTop: -3,
    marginBottom: 10,
    padding: 10,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#ddd'
  },
  box__detailRow: {
    flexDirection: 'row',
    marginBottom: 2
  },
  box__detailCol: {
    flex: 2
  },
  box__detailTx: {
    lineHeight: 16
  }
});

export default Box;