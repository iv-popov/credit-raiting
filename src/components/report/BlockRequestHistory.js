import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const BlockRequestHistory = ({request}) => (
  <View style={styles.requestHistory}>
    <View style={styles.requestHistory__col_1}>
      <Text style={styles.requestHistory__date}>{request.date}</Text>
    </View>
    <View style={styles.requestHistory__col_2}>
      <Text style={styles.requestHistory__reason}>{request.reason_for_enquiry}</Text>
      <Text>{request.creditPurpose}</Text>
      <Text>{request.summa}</Text>
      <Text>{request.bank_name}</Text>                                                            
    </View>          
  </View>
);

BlockRequestHistory.propTypes = {
  request: React.PropTypes.object.isRequired
}

const styles = StyleSheet.create({
  requestHistory: {
    flexDirection: 'row',
    marginBottom: 10,
    paddingBottom: 10,
    borderBottomWidth:1,
    borderColor:'#999'
  },
  requestHistory__col_1: {
    flex: 2
  },
  requestHistory__col_2: {
    flex: 4
  },
  requestHistory__date: {
    fontStyle: 'italic'
  },
  requestHistory__reason: {
    color: '#7EC26D',
    fontWeight: '700'
  }
});
export default BlockRequestHistory;