import React, { Component, PropTypes } from 'react';
import { View, Text, TouchableOpacity, Platform, StyleSheet } from 'react-native';
import ArcGauge from './ArcGauge';
import Box from './Box';
import BlockRequestHistory from './BlockRequestHistory'; 
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';

class Rating extends Component {
  static propTypes = {
    onShowForm: PropTypes.func.isRequired,
    reportInfo: PropTypes.object.isRequired
  }

  getDate = () => {
    const { date }  = this.props.reportInfo.report;
    const arrMonth = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"];

    return `${date.getDate()} ${arrMonth[date.getMonth()]} ${date.getFullYear()}`;
  }
  render() {
    const { ratingInfo, report } = this.props.reportInfo;
    const activeLoans = report.contracts.map((contract) => {
      if (contract.active) {
        return (
          <TouchableOpacity key={contract.id} onPress={() => {Actions.creditInfo({contract, bgColor: '#E8F8FC', date: this.getDate()})}} style={{marginBottom: 10}}>
            <Box contract={contract} bgColor="#E8F8FC" />
          </TouchableOpacity>
        );
      }
    });
    const noActiveLoans = report.contracts.map((contract) => {
      if (!contract.active) {
        return (
          <TouchableOpacity key={contract.id} onPress={() => {Actions.creditInfo({contract, bgColor: '#EFF8E9', date: this.getDate()})}} style={{marginBottom: 10}}>
            <Box contract={contract} bgColor="#EFF8E9" />
          </TouchableOpacity>
        );
      }
    });
    const requestCreditHistory = report.requestCreditHistory.map((request) => <BlockRequestHistory request={request} key={request.id} />);
    
    return (
      <View>
        <View style={styles.updateDate}>
          <Text style={styles.updateDateText}>ОБНОВЛЕНО {this.getDate().toUpperCase()}</Text>
        </View>
        <View style={styles.ratingView}>
          <ArcGauge
            value={83.2} 
            size={20}
            radius={130}
            sections={["#FF0000", "#ff6800", "#ff8e00", "#f8f32b", "#ffff00", "#edff21", "#bfff00", "#8DEA00", "#00C518" ]}
            arrow={{height: 80, width: 8, color: "#515151"}}
            legend={['400', '500', '640', '700', '800', '880', '1000', '1200', '1400']}
          />
          <Text style={styles.ratingIndicator}>Мой кредитный рейтинг: {report.ratingIndicator}{"\n"}</Text>
          <Text style={styles.ratingText}>{ratingInfo.text}</Text>
        </View>
        <View style={styles.btnContainer}>
          <Icon.Button name={Platform.OS === 'ios' ? 'ios-sync' : 'md-sync'} backgroundColor="#299E30" onPress={this.props.onShowForm}>
            Обновить кредитную историю
          </Icon.Button>
        </View>
        <Text style={styles.heading}>Активные кредиты и кредитные карты</Text>
        {activeLoans}
        <Text style={styles.heading}>Закрытые кредиты</Text>
        {noActiveLoans}
        <Text style={styles.heading}>Кто запрашивал мою кредитную историю</Text>
        {requestCreditHistory}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15,
    marginTop: 70,
    ...Platform.select({
      ios: {
        marginBottom: 50
      }
    })
  },
  heading: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10
  },
  btnContainer: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: 'center'
  },
  updateDate: {
    borderBottomWidth: 1,
    borderColor: '#999',
    marginVertical: 10,
    paddingBottom: 5
  },
  updateDateText: {
    fontWeight: '700'
  },
  ratingView: {
    alignItems: 'center'
  },
  ratingIndicator: {
    fontSize: 16
  },
  ratingText: {
    textAlign: 'center',
    fontWeight: '700'
  },
});

export default Rating;