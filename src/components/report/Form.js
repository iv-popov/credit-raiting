import React, { Component, PropTypes } from 'react';
import { View, Text, TextInput, Alert, Platform, StyleSheet } from 'react-native';
import isEmpty from 'lodash/isEmpty'; 

import Icon from 'react-native-vector-icons/Ionicons';

class Form extends Component {
  state = {
    surname: '',
    name: '',
    patronymic: '',
    passportSeries: '',
    passportNumbers: '',
  }

  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onHideForm: PropTypes.func.isRequired
  };

  getCreditHistory = () => {
    const valid = this.validationForm();

    if (valid) {
      this.props.onSubmit();
    } else {
      Alert.alert(
        'Ошибка',
        'Пожалуйста заполните все поля',
        [
          {text: 'OK'}
        ]
      );
    }
  }

  validationForm = () => {
    return Object.keys(this.refs).every(name => !isEmpty(this.state[name]));
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputGroup}>
          <Text>Фамилия</Text>
          <View style={styles.inputContainer}>
            <TextInput
              ref='surname'
              style={styles.textInput}
              placeholder="Введите фамилию"
              placeholderTextColor="#000"
              underlineColorAndroid="transparent"
              onChangeText={(surname) => this.setState({surname})}
              value={this.state.surname}
            />
          </View>
        </View>
        <View style={styles.inputGroup}>
          <Text>Имя</Text>
          <View style={styles.inputContainer}>
            <TextInput
              ref='name'
              style={styles.textInput}
              placeholder="Введите имя"
              placeholderTextColor="#000"
              underlineColorAndroid="transparent"
              onChangeText={(name) => this.setState({name})}
              value={this.state.name}
            />
          </View>
        </View>
        <View style={styles.inputGroup}>
          <Text>Отчество</Text>
          <View style={styles.inputContainer}>
            <TextInput
              ref='patronymic'
              style={styles.textInput}
              placeholder="Введите отчество"
              placeholderTextColor="#000"
              underlineColorAndroid="transparent"
              onChangeText={(patronymic) => this.setState({patronymic})}
              value={this.state.patronymic}
            />
          </View>
        </View>
        <View style={styles.inputGroup}>
          <Text>Серия и номер паспорта</Text>
          <View style={{flexDirection: 'row'}}>
            <View style={[styles.inputContainer, styles.seriesContainer]}>
              <TextInput
                ref='passportSeries'
                style={styles.textInput}
                placeholder="Серия"
                placeholderTextColor="#000"
                underlineColorAndroid="transparent"
                keyboardType='numeric'
                onChangeText={(passportSeries) => this.setState({passportSeries})}
                value={this.state.passportSeries}
              />
            </View>
            <View style={[styles.inputContainer, styles.numbersContainer]}>
              <TextInput
                ref='passportNumbers'
                style={styles.textInput}
                placeholder="Номер паспорта"
                placeholderTextColor="#000"
                underlineColorAndroid="transparent"
                keyboardType='numeric'
                onChangeText={(passportNumbers) => this.setState({passportNumbers})}
                value={this.state.passportNumbers}
              />
            </View>
          </View>
        </View>
        <View style={styles.btnContainer}>
          <View style={styles.btnLeft}>
            <Icon.Button name="ios-card"  backgroundColor="#299E30" onPress={this.getCreditHistory}>
              Запросить историю
            </Icon.Button>
          </View>
          <View style={styles.btnRight}>
            <Icon.Button name="ios-close"   backgroundColor="#299E30" onPress={this.props.onHideForm}>
              Отмена
            </Icon.Button>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15,
    marginTop: 70,
    ...Platform.select({
      ios: {
        marginBottom: 50
      }
    })
  },
  inputGroup: {
    marginBottom: 15
  },
  inputContainer: {
    height: 30,
    padding: 0,
    borderBottomColor: '#299E30',
    borderBottomWidth: 2
  },
  textInput: {
    paddingVertical: 0,
    paddingHorizontal: 5,
    height: 25,
    fontSize: 16,
    color: '#000'
  },
  seriesContainer: {
    flex: 1,
    marginRight: 15
  },
  numbersContainer: {
    flex: 3
  },
  btnContainer: {
    marginTop: 20,
    flexDirection: 'row'
  },
  btnLeft: {
    flex: 4,
    marginRight: 20
  },
  btnRight: {
    flex: 2,
  }
})

export default Form;