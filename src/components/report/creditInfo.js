import React, { Component } from 'react';
import { View, Text, ScrollView, Animated, TouchableHighlight, Platform, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Box from './Box';
import Collapsible from 'react-native-collapsible';

const creditInfo = ({ contract, bgColor, date }) => (
  <ScrollView style={styles.container}>
    {!contract.active && 
      <View style={styles.creditInfo__warning}>
        <Text style={styles.creditInfo__center}>{contract.closing_reason}</Text>
      </View>
    }
    <Text style={styles.creditInfo__heading}>Кредит {contract.bank_name}</Text>
    <Text style={styles.creditInfo__date}>По состоянию на {date}</Text>
    <Box contract={contract} bgColor={bgColor} isDetail />
  </ScrollView>
);

creditInfo.propTypes = {
  contract: React.PropTypes.object.isRequired,
  bgColor: React.PropTypes.string.isRequired,
  date: React.PropTypes.string.isRequired,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    marginTop: 70,
    ...Platform.select({
      ios: {
        marginBottom: 50
      }
    })
  },
  creditInfo__center: {
    textAlign: 'center'
  },
  creditInfo__warning: {
    padding: 5,
    marginBottom: 10,
    backgroundColor: '#FFFCDD',
    borderWidth: 1,
    borderColor: '#FED77C',
  },
  creditInfo__heading: {
    fontSize: 16,
    fontWeight: '700',
  },
  creditInfo__date: {
    marginBottom: 15,
    fontSize: 12
  }
});

export default creditInfo;