import React, {Component} from 'react';
import { ScrollView, Text, View, Image, BackAndroid, Platform, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';

class Recommendation extends Component {
  render() {
    return (
      <ScrollView  style={styles.container}>
        <Text style={styles.heading}>
          Что влияет на Персональный кредитный рейтинг
        </Text>
        <View style={{flex:1, alignItems: 'center', paddingBottom: 10}}>
          <Image source={require('../recommedation.png')} style={styles.recommendationImage} resizeMode="stretch"/>
        </View>
        <Text style={styles.paragraph}>
          <Text><Text style={styles.percent}>25%</Text> - Показатели исторической просрочки{"\n"}</Text>
          <Text><Text style={styles.percent}>23%</Text> - Особенности кредитного поведения{"\n"}</Text>
          <Text><Text style={styles.percent}>18%</Text> - Показатели недавней или текущей просрочки{"\n"}</Text>
          <Text><Text style={styles.percent}>16%</Text> - Характер и динамика запросов в кредитное бюро{"\n"}</Text>
          <Text><Text style={styles.percent}>14%</Text> - Характеристики объема и динамики кредитной нагрузки{"\n"}</Text>
          <Text><Text style={styles.percent}>4%</Text> - Платежная дисциплина в первые месяцы обслуживания долга</Text>
        </Text>
        <Text style={styles.paragraph}>Как можно исправить плохой кредитный рейтинг</Text>
        <Text style={styles.paragraph}>«Плохой» кредитный рейтинг — один из основных поводов отказа заёмщику в кредите. Исправить кредитный рейтинг нельзя, но можно его улучшить.</Text>
        <Text style={styles.paragraph}>Если Вы хотите улучшить рейтинг с целью взять крупный кредит в будущем, оформите кредитную карту или небольшой потребительский кредит, который Вы будете аккуратно и в срок погашать.</Text>
        <Text style={styles.paragraph}>Таким образом, свежая история своевременных платежей улучшит Вашу репутацию в глазах новых кредиторов.</Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    marginTop: 70,
    ...Platform.select({
      ios: {
        marginBottom: 50
      }
    })
  },
  heading: {
    paddingBottom: 10,
    fontSize: 18,
    fontWeight: '700',
    lineHeight: 20,
  },
  paragraph: {
    paddingBottom: 5
  },
  recommendationImage: {
    flex: 1,
    width: 249,
  },
  percent: {
    color: '#299E30',
    fontWeight: '700',
    fontSize: 15
  }
})

export default Recommendation;