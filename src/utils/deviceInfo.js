import DeviceInfo from 'react-native-device-info';

export default () => ({
  deviceUniqueID: DeviceInfo.getUniqueID(),
  deviceSystemName: DeviceInfo.getSystemName(),
  deviceSystemVersion: DeviceInfo.getSystemVersion(),
  deviceModel: DeviceInfo.getModel()
});