export default function checkHttpStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  return Promise.reject('Произошла системная ошибка, повторите запрос позже!');

  /*const error = new Error(response.statusText);
  error.response = response;
  throw error;*/
}