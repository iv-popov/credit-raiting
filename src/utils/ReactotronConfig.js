import Reactotron from 'reactotron';
import { Platform } from 'react-native';

Reactotron.connect({
  enabled: true,
  name: 'ReactNativeExample',
  userAgent: Platform.OS
})