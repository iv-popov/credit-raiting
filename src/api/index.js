import find from 'lodash/find'; 

const fakeDataBase = {
  users: [
    {
      id: 7124598207
    }
  ],
  ratings: [
    {
      id: 1,
      minIndicator: 0,
      maxIndicator : 400,
      text: 'Очень низкий кредитный рейтинг. Вероятнее всего, Вам откажут в выдаче кредита'
    },
    {
      id: 2,
      minIndicator: 401,
      maxIndicator : 500,
      text: 'Низкий кредитный рейтинг. Вероятнее всего, Вам откажут в выдаче кредита'
    },
    {
      id: 3,
      minIndicator: 501,
      maxIndicator : 640,
      text: 'Кредитный рейтинг ниже среднего. Вероятнее всего, Вам откажут в выдаче кредита'
    },
    {
      id: 4,
      minIndicator: 641,
      maxIndicator : 700,
      text: 'Средний кредитный рейтинг. Вероятнее всего, Вам откажут в выдаче кредита'
    },
    {
      id: 5,
      minIndicator: 701,
      maxIndicator : 800,
      text: 'Средний кредитный рейтинг. Вероятнее всего, Вам откажут в выдаче кредита'
    },
    {
      id: 6,
      minIndicator: 801,
      maxIndicator : 880,
      text: 'Средний кредитный рейтинг. Вероятнее всего, Вам откажут в выдаче кредита'
    },
    {
      id: 7,
      minIndicator: 881,
      maxIndicator : 1000,
      text: 'Высокий кредитный рейтинг. Вероятнее всего Вам одобрят выдачу кредита'
    },
    {
      id: 8,
      minIndicator: 1001,
      maxIndicator : 1200,
      text: 'Отличный кредитный рейтинг. Вероятность отказа в выдаче кредита минимальна'
    },
    {
      id: 9,
      minIndicator: 1201,
      maxIndicator : 1400,
      text: 'Превосходный кредитный рейтинг. Вероятность отказа в выдаче кредита минимальна'
    },
  ],
  reports: {
    7124598207: {
      ratingIndicator: 1100,
      contracts: [
        {
          id: 1,
          bank_name: "Русский Стандарт Банк",
          creditPurpose: "Потребительский кредит",
          summa: '300 000',
          period: '3 года',
          rest: '66 100',
          payment_month: '2 100',
          active: true,
          detailInfo: {
            generalInfo: {
              lender: "ЗАО \"Русский Стандарт Банк\"",
              typeFinancing: "Потребительский кредит",
              typeSecurity: "--",
              typeOwner: "Владелец",
              time: "36 месяцев",
              dateOpen: "01.02.2015",
              dateLastPayment: "--",
              dateMissedPayment: "--",
              dateEnd: "01.02.2018",
              dateClose: "--",
              dateTrial: "--",
              dateDeduction: "--",
              currency: "RUB",
              summa: "300 000",
              payment_month: "2 100",
              rest: "66 100",
              amountDelay: "--"
            },
            source: {
              name: "ЗАО \"Русский Стандарт Банк\"",
              ogrn: 8437383423483,
              inn: 9032943453,
              okpo: 40723483
            }
          }
        },
        {
          id: 2,
          bank_name: "Тинькофф Кредитные Системы",
          creditPurpose: "Кредит на автомобиль",
          summa: '300 000',
          period: '2 года',
          rest: '76 000',
          payment_month: '1 000',
          active: true,
          detailInfo: {
            generalInfo: {
              lender: "Тинькофф Кредитные Системы",
              typeFinancing: "Кредит на автомобиль",
              typeSecurity: "--",
              typeOwner: "Владелец",
              time: "24 месяца",
              dateOpen: "01.02.2015",
              dateLastPayment: "--",
              dateMissedPayment: "--",
              dateEnd: "01.02.2017",
              dateClose: "--",
              dateTrial: "--",
              dateDeduction: "--",
              currency: "RUB",
              summa: "300 000",
              payment_month: "1 000",
              rest: "70 000",
              amountDelay: "--"
            },
            source: {
              name: "Тинькофф Кредитные Системы",
              ogrn: 5937383423483,
              inn: 5432943453,
              okpo: 53723483
            }
          }
        },
        {
          id: 3,
          bank_name: "Райффайзенбанк",
          creditPurpose: "Кредит на автомобиль",
          summa: '150 000',
          period: '3 мес',
          rest: 0,
          payment_month: '60 000',
          active: false,
          closing_reason: 'Досрочное погашение 25 мая 2013',
          detailInfo: {
            generalInfo: {
              lender: "Райффайзенбанк",
              typeFinancing: "Кредит на автомобиль",
              typeSecurity: "--",
              typeOwner: "Владелец",
              time: "3 месяца",
              dateOpen: "25.03.2013",
              dateLastPayment: "--",
              dateMissedPayment: "--",
              dateEnd: "25.06.2013",
              dateClose: "25.05.2013",
              dateTrial: "--",
              dateDeduction: "--",
              currency: "RUB",
              summa: "150 000",
              payment_month: "60 000",
              rest: "--",
              amountDelay: "--"
            },
            source: {
              name: "Райффайзенбанк",
              ogrn: 1937383423483,
              inn: 2432943453,
              okpo: 53723483
            }
          }
        },
        {
          id: 4,
          bank_name: "ВТБ24",
          creditPurpose: "Потребительский кредит",
          summa: '300 000',
          period: '6 лет',
          rest: 0,
          payment_month: '5 000',
          active: false,
          closing_reason: 'Кредит закрыт 3 августа 2012',
          detailInfo: {
            generalInfo: {
              lender: "ВТБ24",
              typeFinancing: "Потребительский кредит",
              typeSecurity: "--",
              typeOwner: "Владелец",
              time: "72 месяца",
              dateOpen: "03.08.2006",
              dateLastPayment: "--",
              dateMissedPayment: "--",
              dateEnd: "03.08.2006",
              dateClose: "03.08.2006",
              dateTrial: "--",
              dateDeduction: "--",
              currency: "RUB",
              summa: "300 000",
              payment_month: "5 000",
              rest: "--",
              amountDelay: "--"
            },
            source: {
              name: "ВТБ24",
              ogrn: 2937383483483,
              inn: 3432943453,
              okpo: 83723483
            }
          }
        }
      ],
      requestCreditHistory:  [
        {
          id: 1,
          bank_name: "ЗАО \"Альфа-Банк\"",
          date: "26 апр 2016",
          reason_for_enquiry: "Проверка клиента",
          creditPurpose: "Потребительский кредит",
          summa: "100 000 RUB"
        },
        {
          id: 2,
          bank_name: "Национальный Банк Траст",
          date: "20 апр 2015",
          reason_for_enquiry: "Проверка клиента",
          creditPurpose: "Кредит на автомобиль",
          summa: "30 000 USD"
        }
      ]
    }
  },
  listReports: [
    /*{
      userId: 7124598207,
      text: 'Текстовая расшифровка рейтинга',
      ratingIndicator: 7
    },
    {
      userId: 1124598207,
      text: 'Текстовая расшифровка рейтинга для второго',
      ratingIndicator: 3
    }*/
  ],
  listRating: [
    {
      ratingIndicator : 1,
      text: 'Текстовая расшифровка для райтинга со значением 1'
    },
    {
      ratingIndicator : 2,
      text: 'Текстовая расшифровка для райтинга со значением 2'
    },
    {
      ratingIndicator : 3,
      text: 'Текстовая расшифровка для райтинга со значением 3'
    },
    {
      ratingIndicator : 4,
      text: 'Текстовая расшифровка для райтинга со значением 4'
    },
    {
      ratingIndicator : 5,
      text: 'Текстовая расшифровка для райтинга со значением 5'
    },
    {
      ratingIndicator : 6,
      text: 'Текстовая расшифровка для райтинга со значением 6'
    },
    {
      ratingIndicator : 7,
      text: 'Текстовая расшифровка для райтинга со значением 7'
    },
    {
      ratingIndicator : 8,
      text: 'Текстовая расшифровка для райтинга со значением 8'
    },
    {
      ratingIndicator : 9,
      text: 'Текстовая расшифровка для райтинга со значением 9'
    },
    {
      ratingIndicator : 10,
      text: 'Текстовая расшифровка для райтинга со значением 10'
    }
  ]
} 

const delay = (ms) => 

  new Promise(resolve => setTimeout(resolve, ms));

export const fetchReport = (userId) => 
  delay(1000).then(() =>  find(fakeDataBase.listReports, {userId: Number(userId)}));

export const fetchRatingInfo = () => delay(1000).then(() => fakeDataBase.reports[7124598207]);

export const fetchGuideRaitings = () => delay(1000).then(() => fakeDataBase.ratings);
  