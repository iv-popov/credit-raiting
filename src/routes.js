import React, { Component } from 'react';
import { Platform, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Router, Scene, Actions, Switch } from 'react-native-router-flux';
import AuthLogin from './components/auth/AuthLogin';
import AuthCaptcha from './components/auth/AuthCaptcha';
import AuthLoginConfirm from './components/auth/AuthLoginConfirm';
import AuthCreatePin from './components/auth/AuthCreatePin';
import Login from './components/auth/Login';
import Home from './components/Home';
import Report from './containers/Report';
import creditInfo from './components/report/creditInfo';
import Recommendation from './components/Recommendation';
import NavigationDrawer from './components/NavigationDrawer';
import Icon from 'react-native-vector-icons/Ionicons';
import TabIcon from './components/TabIcon';

// const ReduxRouter = connect()(Router);
const ReduxRouter = Router;
class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      barsIcon: []
    }
  }
  componentWillMount() {
    const logOutIcon = Platform.OS === 'ios' ? 'ios-log-out' : 'md-log-out';
    const arrowBackIcon = Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back';
    Promise.all([
      Icon.getImageSource(logOutIcon, 25, 'white'),
      Icon.getImageSource(arrowBackIcon, 25, 'white'),
    ]).then(results => {
      this.setState({ barsIcon: [results[0], results[1]] })
    });
  }

  getKey(props) {
    if (props.isAuthentificatedApp) {
      return props.isAuthentificatedUser ? 'home' : 'loginUser';
    }

    return 'auth';
  }
  
  render() {
    if(!this.state.barsIcon.length) return null;
    
    return (
      <ReduxRouter>
        <Scene key="root"
               component={connect(state=>({auth:state.auth}))(Switch)}
               tabs
               selector={props => this.getKey(props.auth)}
        >
          <Scene key="auth">
            <Scene key="login" component={AuthLogin} title="Авторизация приложения" hideNavBar panHandlers={null} duration={0}/>
            <Scene key="captcha" component={AuthCaptcha} title="Авторизация" hideNavBar panHandlers={null} duration={0}/>
            <Scene key="loginConfirm" component={AuthLoginConfirm} title="Подтверждение авторизации" hideNavBar panHandlers={null} duration={0}/>
            <Scene key="createPin" component={AuthCreatePin} title="Установка PIN-кода для входа" hideNavBar panHandlers={null} duration={0}/>
          </Scene>
          <Scene key="loginUser" component={Login} title="Авторизация" hideNavBar panHandlers={null} duration={0}/>
          <Scene key="home" component={Platform.OS === 'ios' ? null : NavigationDrawer} duration={0}>
            <Scene key="wrapper" tabs hideNavBar tabBarStyle={styles.tabBarStyle}>
              <Scene 
                key="main"
                component={Home}
                title="Главная"
                icon={Platform.OS === 'ios' ? TabIcon : null}
                iconName={"ios-home-outline"}
                iconNameActive={"ios-home"}
                duration={0}
                navigationBarStyle={styles.tabBarStyle}
                titleStyle={styles.barTitleStyle}
                drawerImage={require('./hamburger.png')}
                onRight={this.props.onLogoutUser}
                rightButtonImage={this.state.barsIcon[0]}
                rightButtonIconStyle={styles.barRightIconStyle}
              />
              <Scene
                key="report"
                duration={0}
                icon={Platform.OS === 'ios' ? TabIcon : null}
                iconName={"ios-paper-outline"}
                iconNameActive={"ios-paper"}
                title="Отчет"
                navigationBarStyle={styles.tabBarStyle}
                titleStyle={styles.barTitleStyle}
                drawerImage={require('./hamburger.png')}
                onRight={this.props.onLogoutUser}
                backButtonImage={this.state.barsIcon[1]}
                leftButtonIconStyle={styles.backButton}
                rightButtonImage={this.state.barsIcon[0]}
                rightButtonIconStyle={styles.barRightIconStyle}
              >
                <Scene
                  key="reportMain"
                  component={Report}
                  title="Отчет"
                />
                <Scene
                  key="creditInfo"
                  component={creditInfo}
                  title="Информация по кредиту"
                />
              </Scene>
              <Scene
                key="recommendation"
                component={Recommendation}
                title="Рекомендации"
                icon={Platform.OS === 'ios' ? TabIcon : null}
                iconName={"ios-information-circle-outline"}
                iconNameActive={"ios-information-circle"}
                navigationBarStyle={styles.tabBarStyle}
                titleStyle={styles.barTitleStyle}
                duration={0}
                drawerImage={require('./hamburger.png')}
                onRight={this.props.onLogoutUser}
                rightButtonImage={this.state.barsIcon[0]}
                rightButtonIconStyle={styles.barRightIconStyle}
              />
            </Scene>
          </Scene>
        </Scene>
      </ReduxRouter>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabBarStyle: {
    backgroundColor: '#299E30'
  },
  barTitleStyle: {
    color: '#fff'
  },
  barRightIconStyle: {
    width: 25,
    height: 25
  },
  backButton: {
    ...Platform.select({
      ios: {
        width: 12
      },
      android: {
        width: 22
    }})
  }
});

export default Routes;