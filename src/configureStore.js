import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import reducers from './reducers';
import { autoRehydrate } from 'redux-persist';

const logger = createLogger();

export default function configureStore() {
  const store = compose(
    autoRehydrate(),
    applyMiddleware(thunk, logger)
  )(createStore)(reducers);

  if (module.hot) {
    // Enable hot module replacement for reducers
    module.hot.accept(() => {
      const nextRootReducer = require('./reducers/index').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};