/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
global.Buffer = global.Buffer || require('buffer').Buffer;

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  TouchableHighlight,
  Alert
} from 'react-native';
import Root from './src/root';

AppRegistry.registerComponent('creditraiting', () => Root);